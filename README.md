# Autostart X11 GUI for Linux

## Purpose

This is a simple BASH script which checks to see if a new terminal session is in
a command terminal `TTY` or within a terminal emulator.  If the session is part
of a command terminal, it will prompt the user if a GUI start through `startx`
is desired, or if the user just wants access to the TTY command terminal.

If the user presses anything other than an `n`, then the script will
automatically start the X11 desktop environment.

## Download

    cd ~/
    git clone https://mdill@bitbucket.org/mdill/bash_gui_start.git
    cd ~/bash_gui_start/

## Placement

It is suggested that this script be placed in `/etc/profile.d/` for simplicity
and ease-of-use.  Once this is done, any terminal should prompt the user,
accordingly.

## License

This project is licensed under the BSD License - see the [LICENSE.md](https://bitbucket.org/mdill/bash_gui_start/src/fc4c62331190a50b8faf094eed6d914c2c7ed200/LICENSE.txt?at=master) file for
details.

