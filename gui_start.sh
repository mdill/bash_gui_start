if [[ $EUID == 0 ]]; then  # If we're root
    printf "\e[1;31m  ______  _____   _____  _______\n |_____/ |     | |     |    |\n |    \_ |_____| |_____|    |\n\n\e[0m With great power comes great responsibility\n\n";
elif [[ $0 == "-bash" ]]; then  # If we're NOT root, but we're in TTY
    # Clear the screen, securely
    printf "\033c";

    if [[ -z $TMUX ]]; then
    fortune -s | cowsay -f mischievio;
    echo "Did you know that: "; whatis $(ls /bin | shuf -n 1);
    fi

    if [[ -z $SSH_CLIENT && -z $TMUX ]]; then  # If we're NOT connected to SSH or TMUX
        read -p $'\e[31mDo you wish to start the GUI? [Y/n]\e[0m  ' -n 1 REPLY;

        if [[  $REPLY =~ ^[Nn]$ ]]; then
            printf '\n\e[1;34m%-6s\e[m\n' 'Here is your command terminal...';
        else
            startx;
        fi
    fi
fi

